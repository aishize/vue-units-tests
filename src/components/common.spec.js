import { mount, shallowMount } from '@vue/test-utils'
import common from './common'
import commonChild from './commonChild'

describe('common', () => {
  it('display "pressed" after the button is pressed', () => {
    const wrapper = mount(common, {
      stubs: {
        commonChild: true
      }
    })
    const button = wrapper.find('button')
    button.trigger('click')
    expect(wrapper.html()).toContain('pressed')
  })
})
describe('commonChild', () => {
  it('display "emoh" after the "reverse" button is pressed', () => {
    const wrapper = mount(common)
    wrapper.find(commonChild).vm.$emit('reverse-text')
    expect(wrapper.html()).toContain('emoh')
  })
})