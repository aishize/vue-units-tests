import commonChild from './commonChild'

export default {
  name: 'common',
  components: {
    commonChild
  },
  data: () => ({
    pressed: false,
    text: 'home'
  }),
  methods: {
    press() {
      this.pressed = true
    },
    reverseText() {
      this.text = this.text.split('').reverse().join('')
    }
  },
  template: `
  <div>
    <commonChild
      :text="text"
      @reverse-text="reverseText"
    />
    <button @click="press">test</button>
    <p v-if="pressed"> pressed </p>
  </div>
  `
}
