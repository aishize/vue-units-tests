export default {
 name: 'commonChild',
 props: {
   text: {
     type: String,
     default: ''
   }
 },
 template: `
  <div>
    <p>{{ text }}</p>
    <button @click="$emit('reverse-text')">reverse</button>
  </div>
 `
}
