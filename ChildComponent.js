export default {
  name: 'ChildComponent',
  template: `
  <div>
    <button @click="$emit('custom')">emit</button>
  </div>
  `
}
